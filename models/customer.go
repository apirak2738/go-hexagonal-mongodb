package models

type CustomerRequest struct {
	Name string
	Age  uint
}
type CustomerResponse struct {
	Name string `bson:"name"`
	Age  uint   `bson:"age"`
}

type UpdateCustomerRequest struct {
	ID   string `bson:"id"`
	Name string `bson:"name"`
	Age  uint   `bson:"age"`
}

type UpdateCustomerResponse struct {
	ID   string `bson:"id"`
	Name string `bson:"name"`
	Age  uint   `bson:"age"`
}

type GetCustomerByListRequest struct {
	Name string `json:"name"`
}

type GetCustomerByListResponse struct {
	Age  uint     `json:"age"`
	Name []string `json:"name"`
}
