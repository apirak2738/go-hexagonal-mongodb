package models

type GenerateVersionModel struct {
	Name          string `json:"name"`
	Detail        string `json:"detail"`
	VersionNumber int    `json:"version_number"`
}
