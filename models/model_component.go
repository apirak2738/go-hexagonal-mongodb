package models

import "time"

type ComponetModel struct {
	ID            string    `json:"id,omitempty"`
	AspID         string    `json:"asp_id,omitempty"`
	DataReferName string    `json:"data_refer_name,omitempty"`
	InputType     string    `json:"input_type,omitempty"`
	DataReferType string    `json:"data_refer_type,omitempty"`
	CreatedBy     string    `json:"created_by,omitempty"`
	CreatedAt     time.Time `json:"createdAt,omitempty"`
	UpdatedBy     string    `json:"updated_by,omitempty"`
	UpdatedAt     time.Time `json:"updatedAt,omitempty"`
}
