package models

type ResponseMessage struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
