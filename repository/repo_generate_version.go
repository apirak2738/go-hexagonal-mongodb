package repository

import (
	"context"
	"errors"
	"fmt"
	"go-hexagonal-mongoDB/models"
	"net/http"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type GenerateVersionRepositoryInterface interface {
	REPOCreateGenerateVersion(req *models.GenerateVersionModel) error
	REPODeleteGenerateVersion(objectID string) error
}

type generateVersionRepositoryInterface struct {
	collection *mongo.Collection
}

func NewGenerateVersionInterface(db_gv *mongo.Database) generateVersionRepositoryInterface {
	return generateVersionRepositoryInterface{collection: db_gv.Collection("generate_version")}
}

func (db generateVersionRepositoryInterface) REPOCreateGenerateVersion(req *models.GenerateVersionModel) error {

	res, err := db.collection.InsertOne(context.TODO(), req)
	if err != nil {
		return err
	}
	fmt.Println("res : ", res.InsertedID)

	return nil
}

func (db generateVersionRepositoryInterface) REPODeleteGenerateVersion(objectID string) error {
	documentID, err := primitive.ObjectIDFromHex(objectID)
	if err != nil {
		return err
	}

	result, err := db.collection.DeleteOne(context.TODO(), bson.D{{"_id", documentID}})
	if err != nil {
		return err
	}

	if result.DeletedCount == 0 {
		return errors.New(http.StatusText(404))
	}

	return nil
}
