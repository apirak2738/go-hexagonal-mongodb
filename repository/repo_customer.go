package repository

import (
	"context"
	"errors"
	"go-hexagonal-mongoDB/constant"
	"go-hexagonal-mongoDB/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Repository interface {
	RepSaveCustomer(model models.CustomerRequest) (res models.CustomerResponse, err error)
	RepGetCustomerByID(id string) (res models.CustomerResponse, err error)
	RepUpdateCustomer(req models.UpdateCustomerRequest) (res models.UpdateCustomerResponse, err error)
	RepDeleteCustomer(id string) error
	RepGetCustomerByList(name string) (res []models.CustomerResponse, err error)
}

type repository struct {
	collection *mongo.Collection
}

func NewRepository(db *mongo.Database) repository {
	return repository{collection: db.Collection("customers")}
}

func (rep repository) RepSaveCustomer(req models.CustomerRequest) (res models.CustomerResponse, err error) {
	_, err = rep.collection.InsertOne(context.TODO(), req)
	if err != nil {
		return res, err
	}

	return models.CustomerResponse{
		Name: req.Name,
		Age:  req.Age,
	}, nil
}

func (rep repository) RepGetCustomerByID(id string) (res models.CustomerResponse, err error) {

	// ระบุ ObjectID ของเอกสารที่คุณต้องการค้นหา
	documentID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return res, err
	}

	// สร้างตัวกรองเพื่อค้นหาเอกสารตาม ObjectID
	filter := bson.M{"_id": documentID}

	// Use FindOne to retrieve the document
	err = rep.collection.FindOne(context.TODO(), filter).Decode(&res)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (rep repository) RepUpdateCustomer(req models.UpdateCustomerRequest) (res models.UpdateCustomerResponse, err error) {
	// ระบุ ObjectID ของเอกสารที่คุณต้องการค้นหา
	documentID, err := primitive.ObjectIDFromHex(req.ID)
	if err != nil {
		return res, err
	}

	filter := bson.D{{"_id", documentID}}
	update := bson.D{
		{"$set", bson.D{
			{"name", req.Name},
			{"age", req.Age},
		}},
	}

	// Use FindOne to retrieve the document
	result, err := rep.collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return res, err
	}

	if result.ModifiedCount == 0 {
		return res, errors.New(constant.MESSAGE_ERROR_NOT_FOUND)
	}

	return models.UpdateCustomerResponse{
		ID:   req.ID,
		Name: req.Name,
		Age:  req.Age,
	}, nil
}

func (rep repository) RepDeleteCustomer(id string) error {

	documentID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	flate := bson.D{{"_id", documentID}}
	result, err := rep.collection.DeleteOne(context.TODO(), flate)
	if err != nil {
		return err
	}

	if result.DeletedCount == 0 {
		return errors.New(constant.MESSAGE_ERROR_NOT_FOUND)
	}

	return nil
}

func (rep repository) RepGetCustomerByList(name string) (res []models.CustomerResponse, err error) {

	// สร้างตัวกรองเพื่อค้นหาเอกสารตาม ObjectID
	filter := bson.M{}

	cursor, err := rep.collection.Find(context.TODO(), filter)
	if err != nil {
		return res, err
	}

	if err = cursor.All(context.TODO(), &res); err != nil {
		panic(err)
	}

	return res, nil
}
