package repository

import (
	"context"
	"fmt"
	"go-hexagonal-mongoDB/models"

	"go.mongodb.org/mongo-driver/mongo"
)

type ComponentRepositoryInterface interface {
	CreateComponnet(req *models.ComponetModel) error
}

type componentRepositoryInterface struct {
	conllection *mongo.Collection
}

func NewComponentRepository(db *mongo.Database) componentRepositoryInterface {
	return componentRepositoryInterface{conllection: db.Collection("component")}
}

func (db *componentRepositoryInterface) CreateComponnet(req *models.ComponetModel) error {
	res, err := db.conllection.InsertOne(context.TODO(), req)
	if err != nil {
		return err
	}
	fmt.Println("res : ", res.InsertedID)

	return nil
}
