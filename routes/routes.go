package routes

import (
	"go-hexagonal-mongoDB/handler"
	"go-hexagonal-mongoDB/repository"
	"go-hexagonal-mongoDB/service"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

func RegistorRouter(db *mongo.Database, apiV1 *gin.RouterGroup) {

	repoComponent := repository.NewComponentRepository(db)
	svComponent := service.NewComponentService(&repoComponent)
	handler.NewComponentHandler(svComponent, apiV1)

	repoGenerateVersion := repository.NewGenerateVersionInterface(db)
	svGenerateVersion := service.NewGenerateVersionService(&repoGenerateVersion)
	handler.NewGenerateVersionHandler(svGenerateVersion, apiV1)

	// repository := repository.NewRepository(db)
	// service := service.NewService(repository)
	// handler.NewHandler(service)

	// customer := e.Group("customers/")
	// customer.POST("create", handler.CreateCustomer)
	// customer.GET(":id", handler.GetCustomerByID)
	// customer.PUT("update", handler.UpdateCustomer)
	// customer.DELETE("delete/:id", handler.DeleteUpdate)
	// customer.POST("list", handler.GetCustomerByList)

}
