package service

import (
	"go-hexagonal-mongoDB/models"
	"go-hexagonal-mongoDB/repository"

	"github.com/gin-gonic/gin"
)

type GenerateVersionServiceInterface interface {
	SVCreateGenerateVersion(c *gin.Context, req *models.GenerateVersionModel) error
	SVDeleteGenerateVersion(c *gin.Context, objectID string) error
}

type generateVersionServiceInterface struct {
	sv repository.GenerateVersionRepositoryInterface
}

func NewGenerateVersionService(sv repository.GenerateVersionRepositoryInterface) GenerateVersionServiceInterface {
	return generateVersionServiceInterface{sv: sv}
}

func (service generateVersionServiceInterface) SVCreateGenerateVersion(c *gin.Context, req *models.GenerateVersionModel) error {

	err := service.sv.REPOCreateGenerateVersion(req)
	if err != nil {
		return err
	}

	return nil
}

func (service generateVersionServiceInterface) SVDeleteGenerateVersion(c *gin.Context, objectID string) error {

	err := service.sv.REPODeleteGenerateVersion(objectID)
	if err != nil {
		return err
	}

	return nil
}
