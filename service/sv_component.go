package service

import (
	"go-hexagonal-mongoDB/models"
	"go-hexagonal-mongoDB/repository"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type ComponentServiceInterface interface {
	SVCreateComponet(c *gin.Context, req *models.ComponetModel) error
}

type componentServiceInterface struct {
	repo repository.ComponentRepositoryInterface
}

func NewComponentService(repo_com repository.ComponentRepositoryInterface) componentServiceInterface {
	return componentServiceInterface{
		repo: repo_com,
	}
}

func (sv componentServiceInterface) SVCreateComponet(c *gin.Context, req *models.ComponetModel) error {

	model := models.ComponetModel{
		ID:    uuid.New().String(),
		AspID: req.AspID,
		// DataReferName: req.DataReferName,
		// InputType:     req.InputType,
		// DataReferType: req.DataReferType,
		// CreatedBy:     "zero",
		// CreatedAt:     time.Now(),
		// UpdatedBy:     "update_zero",
	}

	err := sv.repo.CreateComponnet(&model)
	if err != nil {
		return nil
	}

	return nil
}
