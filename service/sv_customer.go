package service

import (
	"fmt"
	"go-hexagonal-mongoDB/models"
	"go-hexagonal-mongoDB/repository"
)

type Service interface {
	SvCreateCustomer(models.CustomerRequest) (res models.CustomerResponse, err error)
	SvGetCustomerByID(id string) (res models.CustomerResponse, err error)
	SvUpdateCustomer(req models.UpdateCustomerRequest) (res models.UpdateCustomerResponse, err error)
	SvDeleteCustomer(id string) error
	SvGetCustomerByList(req models.GetCustomerByListRequest) (res []models.GetCustomerByListResponse, err error)
}

type service struct {
	rep repository.Repository
}

func NewService(rep repository.Repository) service {
	return service{rep: rep}
}

func (sv service) SvCreateCustomer(req models.CustomerRequest) (res models.CustomerResponse, err error) {

	res, err = sv.rep.RepSaveCustomer(req)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (sv service) SvGetCustomerByID(id string) (res models.CustomerResponse, err error) {

	res, err = sv.rep.RepGetCustomerByID(id)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (sv service) SvUpdateCustomer(req models.UpdateCustomerRequest) (res models.UpdateCustomerResponse, err error) {

	res, err = sv.rep.RepUpdateCustomer(req)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (sv service) SvDeleteCustomer(id string) error {

	err := sv.rep.RepDeleteCustomer(id)
	if err != nil {
		return err
	}

	return nil
}

func (sv service) SvGetCustomerByList(req models.GetCustomerByListRequest) (res []models.GetCustomerByListResponse, err error) {

	list, err := sv.rep.RepGetCustomerByList(req.Name)
	if err != nil {
		return res, err
	}

	dataMap := make(map[uint][]string)
	for _, v := range list {
		dataMap[v.Age] = append(dataMap[v.Age], v.Name)
	}
	fmt.Println("dataMap : ", dataMap)

	for _, v := range dataMap {
		fmt.Println("vvvvvvvvv : ", v)
	}

	return res, err
}
