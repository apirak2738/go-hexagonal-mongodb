package main

import (
	"go-hexagonal-mongoDB/connection"
	"go-hexagonal-mongoDB/routes"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {

	db := connection.ConnectDatabaseMongoDB()

	e := gin.Default()

	apiV1 := e.Group("/api/v1")

	// e.NoRoute(func(ctx *gin.Context) {
	// 	ctx.JSON(http.StatusNotFound, models.ResponseMessage{
	// 		Message: "url not found",
	// 	})
	// })

	routes.RegistorRouter(db, apiV1)

	// Start listening and serving requests
	err := e.Run(":8080")
	if err != nil {
		log.Fatalf("impossible to start server: %s", err)
	}

}
