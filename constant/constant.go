package constant

// Message Error
const (
	MESSAGE_ERROR_NOT_FOUND string = "data not found"
)

// Success Error
const (
	MESSAGE_SUCCESS string = "success"
)
