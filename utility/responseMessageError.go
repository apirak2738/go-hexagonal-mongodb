package utility

import (
	"go-hexagonal-mongoDB/constant"
	"go-hexagonal-mongoDB/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ResponseMessage(c *gin.Context, response interface{}, err error) {

	res := models.ResponseMessage{}

	if err == nil {
		res.Message = constant.MESSAGE_SUCCESS
		res.Data = response
		c.JSON(http.StatusOK, res)
		return
	} else if err.Error() == constant.MESSAGE_ERROR_NOT_FOUND {
		res.Message = err.Error()
		c.JSON(http.StatusNotFound, res)
		return
	} else {
		res.Message = err.Error()
		c.JSON(http.StatusInternalServerError, res)
		return
	}
}
