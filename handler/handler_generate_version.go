package handler

import (
	"go-hexagonal-mongoDB/models"
	"go-hexagonal-mongoDB/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type generateVersionHandler struct {
	sv service.GenerateVersionServiceInterface
}

func NewGenerateVersionHandler(sv service.GenerateVersionServiceInterface, apiV1 *gin.RouterGroup) {

	h := generateVersionHandler{sv: sv}
	routes := apiV1.Group("/generate_version")
	routes.POST("/create", h.GetCreateGenerateVersion)
	routes.DELETE("/delete", h.DeleteGenerateVersion)

}

func (h generateVersionHandler) GetCreateGenerateVersion(c *gin.Context) {

	req := new(models.GenerateVersionModel)

	if err := c.ShouldBind(&req); err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	err := h.sv.SVCreateGenerateVersion(c, req)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, "success")
}

func (h generateVersionHandler) DeleteGenerateVersion(c *gin.Context) {

	objectID := c.Query("object_id")

	if objectID == "" {
		c.JSON(http.StatusBadRequest, "objectID")
		return
	}

	err := h.sv.SVDeleteGenerateVersion(c, objectID)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, "success")
}
