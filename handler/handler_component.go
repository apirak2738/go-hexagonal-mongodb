package handler

import (
	"go-hexagonal-mongoDB/models"
	"go-hexagonal-mongoDB/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type componentHandler struct {
	sv service.ComponentServiceInterface
}

func NewComponentHandler(sv service.ComponentServiceInterface, apiV1 *gin.RouterGroup) {
	h := componentHandler{sv: sv}
	group := apiV1.Group("/component")
	group.POST("/create", h.CreateComponent)

}

func (h *componentHandler) CreateComponent(c *gin.Context) {

	req := new(models.ComponetModel)

	if err := c.ShouldBind(&req); err != nil {
		c.JSON(http.StatusBadRequest, "err")
		return
	}

	err := h.sv.SVCreateComponet(c, req)
	if err != nil {
		c.JSON(http.StatusBadRequest, "err")
		return
	}

	c.JSON(http.StatusOK, "")
}
