package handler

import (
	"fmt"
	"go-hexagonal-mongoDB/models"
	"go-hexagonal-mongoDB/service"
	"go-hexagonal-mongoDB/utility"

	"github.com/gin-gonic/gin"
)

type handler struct {
	sv service.Service
}

func NewHandler(sv service.Service) handler {

	return handler{sv: sv}
}

func (h handler) CreateCustomer(c *gin.Context) {

	req := models.CustomerRequest{}

	if err := c.ShouldBind(&req); err != nil {
		utility.ResponseMessage(c, nil, err)
		return
	}

	res, err := h.sv.SvCreateCustomer(req)
	if err != nil {
		utility.ResponseMessage(c, nil, err)
		return
	}

	utility.ResponseMessage(c, res, err)

}

func (h handler) GetCustomerByID(c *gin.Context) {

	res, err := h.sv.SvGetCustomerByID(c.Param("id"))
	if err != nil {
		utility.ResponseMessage(c, nil, err)
		return
	}

	utility.ResponseMessage(c, res, err)

}

func (h handler) UpdateCustomer(c *gin.Context) {

	req := models.UpdateCustomerRequest{}

	if err := c.ShouldBind(&req); err != nil {
		utility.ResponseMessage(c, nil, err)
		return
	}

	updateCustomer, err := h.sv.SvUpdateCustomer(req)
	if err != nil {
		utility.ResponseMessage(c, nil, err)
		return
	}

	utility.ResponseMessage(c, updateCustomer, err)
}

func (h handler) DeleteUpdate(c *gin.Context) {

	err := h.sv.SvDeleteCustomer(c.Param("id"))
	if err != nil {
		utility.ResponseMessage(c, nil, err)
		return
	}
	utility.ResponseMessage(c, nil, err)
}

func (h handler) GetCustomerByList(c *gin.Context) {

	req := models.GetCustomerByListRequest{}

	if err := c.ShouldBind(&req); err != nil {
		utility.ResponseMessage(c, nil, err)
		return
	}
	fmt.Println("req : ", req)

	res, err := h.sv.SvGetCustomerByList(req)
	if err != nil {
		utility.ResponseMessage(c, nil, err)
		return
	}
	utility.ResponseMessage(c, res, err)
}
